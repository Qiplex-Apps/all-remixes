<div align="center">

<h1> <a href="https://qiplex.com/software/all-remixes/">All Remixes</a> </h1>

<h3> Discover all remixes of the songs you love! </h3>

You can get the app on 
<a href="https://gitlab.com/Qiplex-Apps/all-remixes/-/releases">GitLab</a>
or on 
<a href="https://qiplex.com/software/all-remixes/">my website</a>
<br>Code comes soon.

![All Remixes](https://qiplex.com/assets/img/app/main/all-remixes-app.png)

<h4> Check out the app features below: </h4>

![All Remixes - Features](https://user-images.githubusercontent.com/32670415/147221701-4f66722c-0295-4ede-bba5-abf21fe24a74.png)

</div>
